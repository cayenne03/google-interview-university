**Knowledge Graph** - Structures the world's information in a vast database

**Semantic Web** - Processing multiple databases into a format that can be read as if all the information resides in a single repository
